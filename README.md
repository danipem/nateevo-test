# INSTRUCCIONES
1. Instala node (v14.17.4) a nivel global: https://nodejs.org/download/release/v14.17.4/
2. En consola, en la raíz del proyecto, ejecuta el comando "npm install". Esto instalará todas las dependencias necesarias
3. En la misma ruta, ejecuta el comando "gulp". Esto levantará el proyecto, abriendo una ventana del navedador que se refrescará con cada cambio que realices.

