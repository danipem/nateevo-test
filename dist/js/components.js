
const button = document.querySelector('#read-more')
const hiddenText = document.querySelector('#text-to-hide')

button.addEventListener('click', () => {
    hiddenText.classList.toggle('hidden')
    hiddenText.classList.contains('hidden') ? button.textContent = 'Leer más' : button.textContent = 'Ocultar'
})
const sliderContainer = document.querySelector('.slider__container')
const prevArrow = document.querySelector('.slider__prev-arrow')
const nextArrow = document.querySelector('.slider__next-arrow')
const paginationItems = document.querySelectorAll('.slider__pagination--item')

let currentPage = 0

const showPage = (pageIndex) => {
  sliderContainer.style.transform = `translateX(-${pageIndex * 100}%)`
  paginationItems.forEach((item, index) => item.classList.toggle('active', index === pageIndex))
  currentPage = pageIndex

  paginationItems.forEach((item, index) => item.classList.toggle('white_background', index === pageIndex))
}

prevArrow.addEventListener('click', () => {
  if (currentPage > 0) showPage(currentPage - 1)
})

nextArrow.addEventListener('click', () => {
  if (currentPage < paginationItems.length - 1) showPage(currentPage + 1)
})

paginationItems.forEach((item, index) => item.addEventListener('click', () => showPage(index)))


const langButtons = document.querySelectorAll('.languages__button')
const menu = document.querySelector('.menu')
const hamburger = document.querySelector('.hamburger')

langButtons.forEach(button => {
    button.addEventListener('click', () => {
        langButtons.forEach(otherButton => {
            otherButton === button ?
                button.classList.add('languages__selected')
                : otherButton.classList.remove('languages__selected')
        })
    })
})

const toggleMenu = () => menu.classList.toggle('display-flex')

hamburger.addEventListener('click', () => toggleMenu())

document.addEventListener('click', (event) => {
    const target = event.target
    if (!target.closest('.hamburger') && !target.closest('.menu')) {
        menu.classList.remove('display-flex')
    }
})
