const langButtons = document.querySelectorAll('.languages__button')
const menu = document.querySelector('.menu')
const hamburger = document.querySelector('.hamburger')

langButtons.forEach(button => {
    button.addEventListener('click', () => {
        langButtons.forEach(otherButton => {
            otherButton === button ?
                button.classList.add('languages__selected')
                : otherButton.classList.remove('languages__selected')
        })
    })
})

const toggleMenu = () => menu.classList.toggle('display-flex')

hamburger.addEventListener('click', () => toggleMenu())

document.addEventListener('click', (event) => {
    const target = event.target
    if (!target.closest('.hamburger') && !target.closest('.menu')) {
        menu.classList.remove('display-flex')
    }
})
