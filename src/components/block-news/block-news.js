const button = document.querySelector('#read-more')
const hiddenText = document.querySelector('#text-to-hide')

button.addEventListener('click', () => {
    hiddenText.classList.toggle('hidden')
    hiddenText.classList.contains('hidden') ? button.textContent = 'Leer más' : button.textContent = 'Ocultar'
})