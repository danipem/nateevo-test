const sliderContainer = document.querySelector('.slider__container')
const prevArrow = document.querySelector('.slider__prev-arrow')
const nextArrow = document.querySelector('.slider__next-arrow')
const paginationItems = document.querySelectorAll('.slider__pagination--item')

let currentPage = 0

const showPage = (pageIndex) => {
  sliderContainer.style.transform = `translateX(-${pageIndex * 100}%)`
  paginationItems.forEach((item, index) => item.classList.toggle('active', index === pageIndex))
  currentPage = pageIndex

  paginationItems.forEach((item, index) => item.classList.toggle('white_background', index === pageIndex))
}

prevArrow.addEventListener('click', () => {
  if (currentPage > 0) showPage(currentPage - 1)
})

nextArrow.addEventListener('click', () => {
  if (currentPage < paginationItems.length - 1) showPage(currentPage + 1)
})

paginationItems.forEach((item, index) => item.addEventListener('click', () => showPage(index)))
